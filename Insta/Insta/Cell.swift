//
//  Cell.swift
//  Insta
//
//  Created by apple on 16/03/15.
//  Copyright (c) 2015 Bhavishya. All rights reserved.
//

import UIKit

class Cell: UITableViewCell {
    // for particulat cell in table view...
    @IBOutlet var username: UILabel!
    @IBOutlet var title: UILabel!
    @IBOutlet var postedImage: UIImageView!
}
