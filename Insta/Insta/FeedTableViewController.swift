//
//  FeedTableViewController.swift
//  Insta
//
//  Created by apple on 16/03/15.
//  Copyright (c) 2015 Bhavishya. All rights reserved.
//

import UIKit

class FeedTableViewController: UITableViewController {
    
    var titles = [String]()
    var usernames = [String]()
    var images = [UIImage]()
    var imageFiles = [PFFile]()
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 227
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var myCell:Cell = self.tableView.dequeueReusableCellWithIdentifier("myCell") as Cell
        myCell.title.text = titles[indexPath.row]
        myCell.username.text = usernames[indexPath.row]
        
        imageFiles[indexPath.row].getDataInBackgroundWithBlock{(imageData: NSData!, error: NSError!) -> Void in
            if error == nil {
                let image = UIImage(data: imageData)
                myCell.postedImage.image = image
            }
            
            }
        myCell.sizeToFit()
        return myCell
    }
    
            override func viewDidLoad() {
            super.viewDidLoad()
                
                var getFollowedUsersQuery = PFQuery(className: "followers")
                getFollowedUsersQuery.whereKey("follower", equalTo: PFUser.currentUser().username)
                
                getFollowedUsersQuery.findObjectsInBackgroundWithBlock {
                    (objects: [AnyObject]!, error: NSError!) -> Void in
                    if error == nil {
                        var followedUser = ""
                        for object in objects {
                            followedUser = object["following"] as String
                            
                            var query = PFQuery(className:"Post")
                            query.whereKey("username", equalTo: followedUser)
                            query.findObjectsInBackgroundWithBlock {
                                (objects: [AnyObject]!, error: NSError!) -> Void in
                                if error == nil {
                                    // The find succeeded.
                                    println("Successfully retrieved \(objects.count) scores.")
                                    // Do something with the found objects
                                    
                                    for object in objects {
                                        self.titles.append(object["Title"] as String)
                                        self.usernames.append(object["username"] as String)
                                        self.imageFiles.append(object["imageFile"] as PFFile)
                                        
                                        self.tableView.reloadData()
                                    }
                                    
                                } else {
                                    // Log details of the failure
                                    println("Error: \(error) \(error.userInfo!)")
                                }
                            }
                        }
                    }
                }
        }
}
