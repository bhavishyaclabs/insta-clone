//
//  ViewController.swift
//  Insta
//
//  Created by apple on 13/03/15.
//  Copyright (c) 2015 Bhavishya. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var signUpActive = true
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    func displayAlert(title:String , error: String) {
        var alert = UIAlertController(title: "error informed", message: error, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { action in
            self.dismissViewControllerAnimated(true, completion: nil)
            
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    @IBOutlet weak var logInOutlet: UIButton!
    @IBOutlet weak var signUpLabel: UILabel!
    @IBOutlet weak var alreadyRegisteredOutlet: UILabel!
    @IBOutlet var username: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet weak var signUpButtonOutlet: UIButton!
    @IBAction func toggleSignUp(sender: AnyObject) {
        if signUpActive == true {
            signUpActive = false
            signUpLabel.text = "Log In!"
            logInOutlet.setTitle("Sign Up", forState: UIControlState.Normal)
            alreadyRegisteredOutlet.text = "Not Registered"
            signUpButtonOutlet.setTitle("Log In", forState: UIControlState.Normal)
        } else {
            signUpActive = true
            signUpLabel.text = "Sign Up!"
            logInOutlet.setTitle("Log In", forState: UIControlState.Normal)
            alreadyRegisteredOutlet.text = "Already Registered"
            signUpButtonOutlet.setTitle("Sign Up", forState: UIControlState.Normal)
        }
    }
    @IBAction func signUp(sender: AnyObject) {
        var error = ""
        if username.text == "" || password.text ==
            "" {
                error = "Please enter your username or password"
        }
        if error != "" {
            displayAlert("Error In Form", error: error)
        } else {
            
            
            activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 50, 50))
            activityIndicator.center = self.view.center
            activityIndicator.hidesWhenStopped = true
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
            view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            UIApplication.sharedApplication().beginIgnoringInteractionEvents()
            
            if signUpActive == true {
                var user = PFUser()
                user.username = username.text
                user.password = password.text
                user.signUpInBackgroundWithBlock {
                    (succeeded: Bool!, signUpError: NSError!) -> Void in
                    
                    self.activityIndicator.stopAnimating()
                    UIApplication.sharedApplication().endIgnoringInteractionEvents()
                    
                    
                    if signUpError == nil {
                        self.performSegueWithIdentifier("jumpToUserTable", sender: self)
                        println("Signed Up")
                    } else {
                        if let errorString = signUpError.userInfo?["error"] as? NSString {
                            error = errorString
                        } else {
                            error = "Please try again later"
                        }
                        
                        self.displayAlert("Could not sign up", error: error)
                    }
                    
                }
            } else {
                
                PFUser.logInWithUsernameInBackground(username.text, password:password.text) {
                    (user: PFUser!, signUpError: NSError!) -> Void in
                    
                    self.activityIndicator.stopAnimating()
                    UIApplication.sharedApplication().endIgnoringInteractionEvents()
                    
                    if signUpError == nil {
                        self.performSegueWithIdentifier("jumpToUserTable", sender: self)
                        println("Logged In")
                    } else {
                        // The login failed. Check error to see why.
                        if let errorString = signUpError.userInfo?["error"] as? NSString {
                            error = errorString
                        } else {
                            error = "Please try again later"
                        }
                        
                        self.displayAlert("Could not sign up", error: error)
                        
                    }
                }
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        println(PFUser.currentUser())
    }
    override func viewDidAppear(animated: Bool) {
        if PFUser.currentUser() != nil {
            self.performSegueWithIdentifier("jumpToUserTable", sender: self)
        }
    }
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
    }
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

